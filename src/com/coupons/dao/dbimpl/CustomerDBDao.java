package com.coupons.dao.dbimpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import com.coupons.beans.Customer;
import com.coupons.dao.CustomerDao;
import com.coupons.exceptions.DaoException;

public class CustomerDBDao implements CustomerDao
{
	private static final String TABLE_NAME = "cpns_customer";
	private static final String CUST_NAME = "cust_name";
	private static final String PASSWORD = "password";
	private static final String EMAIL = "email";
	
	@Override
	public void addCustomer(Customer c) throws DaoException {
		// 1. get a connection (from pool)
		Connection conn = null;
		try {
			conn = getConnection();
			
			// 2. create sql insert
			PreparedStatement stat = conn.prepareStatement(
					// TODO generating SQL statements
					"INSERT INTO " + TABLE_NAME + "(" + CUST_NAME + ", " +
							PASSWORD + ", " + EMAIL + ") "
							+ "VALUES (?, ?, ?)");

			stat.setString(1, c.getName());
			stat.setString(2, c.getPassword());
			stat.setString(3, c.getEmail());
			
			System.out.println("Executing: " + stat.toString());
			stat.executeUpdate();
			
		}  catch (SQLException e) {
		    if (e instanceof SQLIntegrityConstraintViolationException) {
		        // TODO
		    	System.out.println("Duplicate entry");
		    } else {
		        // TODO
		    	System.out.println("Other SQL Exception");
		    }
		}
		finally {
			// 3. release connection
			releaseConnection(conn);
		}
	}

	@Override
	public Customer getCustomer(long id) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeCustomer(Customer c) throws DaoException {
				Connection conn = null;
				try {
					conn = getConnection();

					PreparedStatement stat = conn.prepareStatement(
							"DELETE FROM " + TABLE_NAME + " WHERE "
									+ EMAIL + " = ?");

					stat.setString(1, c.getEmail());
					
					System.out.println("Executing: " + stat.toString());
					stat.executeUpdate();
					
				} catch (SQLException e) {
					// TODO: deal with exception
					e.printStackTrace();
				}
				finally {
					// 3. release connection
					releaseConnection(conn);
				}
	}

	@Override
	public void updateCustomer(Customer c) throws DaoException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Customer> getAllCustomers() throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}
	
	private Connection getConnection() throws SQLException
	{
		// TODO: maybe we should catch the exception here
		// and close the program. It is too severe
		String dbName = "coupons";
		String url = "jdbc:mysql://localhost/" + dbName;
		return DriverManager.getConnection(url, "root", "");
	}
	
	private void releaseConnection(Connection conn)
	{
		// We currently are closing the connections. Later we
		// will move to a better solution using a connection pool
		// that Assaf will provide
		try {
			conn.close();
		} catch (SQLException e) {
			// We don't care
			e.printStackTrace();
		}
	}
	
}
