package com.coupons.beans;

public enum CouponType {
	RESTAURANS,
	ELECTRICY,
	FOOD,
	HEALTH,
	SPORTS,
	CAMPING,
	TRAVELLING
}
