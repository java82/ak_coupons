package com.coupons.beans;

import java.util.Date;

public class Coupon {
	//
	// Attributes
	//
	private long id;
	private String title;
	private Date startDate;
	private Date endDate;
	private int amount;
	private CouponType type;
	private String message;
	private double price;
	private String image;
	
	
	//
	// Constructor
	//
	public Coupon() {}
	
	public Coupon(String title, Date startDate, Date endDate, int amount,
		CouponType type, String message, double price, String image) {
		
		this.title = title;
		this.startDate = startDate;
		this.endDate = endDate;
		this.amount = amount;
		this.type = type;
		this.message = message;
		this.price = price;
		this.image = image;
	}
	
	
	//
	// Methods
	//
	/**
	 *
	 * @return
	 */
	public String getCoupon() {
		return this.title;
	}
	
	/**
	 * 
	 */
	public void setCoupon() {
		// TODO
	}
	
	/**
	 * Get info about specific coupon:
	 * ID, title, start date, end date, type and price
	 */
	@Override
	public String toString() {
		String couponInfo = "Coupon ID: " + this.id + "\n"
				+ "Coupon title: " + this.title + "\n" 
				+ "Valid from: " + this.startDate + " till: " + this.endDate + "\n"
				+ "Category: " + this.type + "\n"
				+ "Price: " + this.price;
		return couponInfo;
	}
	
	
}
